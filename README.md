A .Net client for the [Consul](http://consul.io) service discovery system.

[![Build Status](https://ci.appveyor.com/api/projects/status/ssrcb3r1dgiao5xk/branch/master?svg=true)](https://ci.appveyor.com/project/AlecLanter/consulclient/branch/master)  [![NuGet version](https://badge.fury.io/nu/ConsulClient.svg)](https://badge.fury.io/nu/ConsulClient)

# Overview

## To register a service with the agent
```csharp
// Create a new consul client for an agent running at http://localhost:8500
var consul = new Consul();

// Create our health check
var check = new CheckRegistrationInfo
{
	CheckID = "service:someService",
	HTTP = $"http://{Environment.MachineName}:{MyPort}/SomeService/healthCheck",
	DeregisterCriticalServiceAfter = "30m"
};

// Create our catalog information
var svcReg = new ServiceRegistrationInfo
{
	Name = "SomeService",
	ID = "SomeService",
	Address = Environment.MachineName,
	Port = MyPort,
	Check = check,
	Tags = "v1"
};

// Perform registration
var result = await consul.RegisterServiceAsync(svcReg);

if (!result) throw new Exception("Could not register");
```

## To find registered services
```csharp
var consul = new Consul();
var services = await consul.GetServicesAsync("SomeService", "v1");
```

# NuGet
> Install-Package ConsulClient
